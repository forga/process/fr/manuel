EDITOR = geany

clean:
	# Remove files not in source control
	find . -type f -name "*.orig" -delete
	find . -type d -name "site" -delete

open_all:
	${EDITOR} .gitlab-ci.yml Makefile mkdocs.yml README.md requirements.txt
	${EDITOR} ${VIRTUAL_ENV}/bin/activate
	${EDITOR} docs/index.md
	${EDITOR} docs/about/*.md
	${EDITOR} docs/convention/*.md
	${EDITOR} docs/human/*.md
