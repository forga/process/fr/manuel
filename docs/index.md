Manuel
======

Ce document fait parti du groupe [`forga`](https://gitlab.com/forga/), pour plus d'informations sur ce groupe, visitez l'URL : [https://forga.gitlab.io/fr/](https://forga.gitlab.io/fr/)

---

Vous êtes sur la page d'accueil de notre source de documentation **unique**.
Les pages sont **accessibles** et **éditables** _par tous_.

Vous y trouverez les conventions de travail et autres bonnes pratiques de collaboration de notre organisation.

Ce sont des **choix** faits dans un **contexte**, pas des règles immuables, donc si certaines consignes s'avèrent moins pertinente et/ou claire : chacun·e doit pouvoir proposer une
[correction, suppression ou un ajout](https://gitlab.com/-/ide/project/forga/process/fr/manuel/blob/production/-/docs/).

Toutes ces conventions doivent être applicables et appliquées indifféremment des projets, sinon elles doivent être modifiées ou supprimées :

> On écrit ce que l'on fait et on fait ce que l'on écrit.


Votre avis compte!
------------------

Ne pas hésiter à [proposer une mise à jour](https://gitlab.com/forga/process/fr/manuel/-/edit/production/docs/index.md) de cette page.
