Conventions python
==================

Quelques conventions dans l'usage de [`python`](https://www.python.org/) dans nos projets.


Tests
-----

### [Django](https://www.djangoproject.com/)

Certain préfixe, suffixe et mots on un sens clairement défini :

* `test_route_*`
   - prend des routes (URLs) en paramètres et implique que la présence de template soit vérifiée dans le test
* `test_route_anon_*`, `test_route_user_*`, `test_route_super_*`, utilise respectivement un _client de test_ :
   1. anonyme
   1. authentifié avec utilisateur non _super-user_
   1. authentifié avec utilisateur _super-user_
* `*_post`, `*_get`
   - utilise spécifiquement une méthode `HTTP GET` ou `POST`
* `test_form_valid_*`, `test_form_not_valid_*`
   - (in)valiation du formulaire
* `test_form_fields_*`
   - champs présents dans un formulaire
* `*_<app_name>_*`
   - le nom de l'application concernée


Votre avis compte!
------------------

Ne pas hésiter à proposer des mises à jour [pour cette page](https://gitlab.com/forga/process/fr/manuel/-/edit/production/docs/convention/python.md)
