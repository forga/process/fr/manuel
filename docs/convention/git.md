Conventions git
===============

Quelques conventions pour l'usage de [`git`](https://git-scm.com/) dans nos projets.


Mode d'emploi plutôt que journal
--------------------------------

L'historique doit être aussi clair qu'une documentation pour permettre de rejouer les étapes successives de la construction du logiciel (avec les meilleures pratiques) et donc comprendre sa construction.

Pour y arriver, ne pas hésiter [à ré-écrire l'historique](https://git-scm.com/book/fr/v2/Utilitaires-Git-R%C3%A9%C3%A9crire-l%E2%80%99historique) (`git rebase -i` par ex.) **avant de pousser** sur la [branche thématique (_feature branch_)](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Travailler-avec-les-branches#s_topic_branch).


Des commits atomiques et testés
-------------------------------

Le message est préfixé avec la convention : [`gitmoji`](https://gitmoji.carloscuesta.me/).

Un bon commit est :

- constitué d'un [_bon message_](https://chris.beams.io/posts/git-commit/)
- [atomique](https://www.freshconsulting.com/insights/blog/atomic-commits/)
- ne casse pas les tests
- et idéalement il ne diminue pas la couverture de test


### Exemple de rédaction ([source](https://chris.beams.io/posts/git-commit/))

```text
Summarize changes in around 50 characters or less

More detailed explanatory text, if necessary. Wrap it to about 72
characters or so. In some contexts, the first line is treated as the
subject of the commit and the rest of the text as the body. The
blank line separating the summary from the body is critical (unless
you omit the body entirely); various tools like `log`, `shortlog`
and `rebase` can get confused if you run the two together.

Explain the problem that this commit is solving. Focus on why you
are making this change as opposed to how (the code explains that).
Are there side effects or other unintuitive consequences of this
change? Here's the place to explain them.

Further paragraphs come after blank lines.

 - Bullet points are okay, too

 - Typically a hyphen or asterisk is used for the bullet, preceded
   by a single space, with blank lines in between, but conventions
   vary here

If you use an issue tracker, put references to them at the bottom,
like this:

Resolves: #123
See also: #456, #789
```


Les branches
------------

* [_branches thématique_](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Travailler-avec-les-branches#s_topic_branch) pour:
   - pour les tickets (_issues_) du projet
* [_branches au long court_](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Travailler-avec-les-branches#_branches_au_long_cours) pour:
   - celles structurant le projet: `docs`, `production`, `stable`, `devel`, …
   - le code _réutilisable_, les applications [Django](https://docs.djangoproject.com/fr/stable/ref/applications/) par ex.


Relier les commits au projet
----------------------------

Faire autant de références possibles vers la gestion de projet de _GitLab_:

* [vers les tickets ](https://docs.gitlab.com/ce/user/project/issues/crosslinking_issues.html#from-commit-messages)
* vers les autres _objets_ GitLab en utilisant le [caractère de raccourci adéquat](https://docs.gitlab.com/ce/user/project/autocomplete_characters.html)
* [fermer automatiquement un ticket](https://docs.gitlab.com/ce/user/project/issues/managing_issues.html#closing-issues-automatically)

On peut aller même plus loin en utilisant les [_quick actions_](https://docs.gitlab.com/ce/user/project/quick_actions.html) qui se déclancheront une fois sur le serveur.


Les branches principales (au long court)
----------------------------------------

- `production` ou `stable` pour le code _ad hoc_. On ne **_commit_ jamais** directement **dessus** : seulement des fusions (_merge_)
- `preprod` ou `devel` pour la qualification du code : les corrections rapides (_quick-fixes_) peuvent y être _commit_ en direct
- `docs` pour la documentation si le projet se prête à une séparation de celle-ci

Sur ces branches, [on ne **ré-écrit jamais** l'historique](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Rebaser-Rebasing#s_rebase_peril)


Les branches de travail
-----------------------

Préfixée par l'id et suivi du _slang_ du ticket concerné, le plus simple est de les créer depuis _GitLab_ : `2-login-implementation`, `5-update-device-settings`, `13-enable-disable-wlan-interface`, …


Les branches provisoires
------------------------

Préfixée par `wip-`, pour pousser du code incomplet/temporaire. Les tests ne seront pas réalisé par l'intégration continue et peuvent donc échouer sur les commits concerné.

On peut en plus préfixer le message de _commit_ : `🚧 WIP:` pour indiguer où en était le dev.


Les branches d'archive
----------------------

Préfixé par `old-`, pour les branches que l'on souhaite la conserver.


Étiquettes de version
---------------------

On ne déploit que des versions ayant un `tag` correspondant au numéro de version, par exemples : `0.2.7`, `1.0.3`, `0.0.12-alpha`, `0.0.21-rc1`, etc…

* suivre la [gestion sémantique de version](https://semver.org/lang/fr/)
* pas de préfixe
* suffixe pour les versions _alpha_, _release candidate_, etc…


Quelques options utiles
-----------------------

Ne pas hésiter à les rajouter en [alias](https://git-scm.com/book/fr/v2/Les-bases-de-Git-Les-alias-Git) :


| Commandes | Action | Documentation |
| :-------: | :----: | :-----------: |
|`git rebase --interactive <hash>^`                                                 |   _réécrire l'historique_|                        [lien](https://git-scm.com/book/fr/v2/Utilitaires-Git-R%C3%A9%C3%A9crire-l%E2%80%99historique#s_changing_multiple)
|`git diff --cached`/`git diff --staged`                                            |   afficher les changements indexés|               [lien](https://git-scm.com/docs/git-diff#Documentation/git-diff.txt-emgitdiffemltoptionsgt--cachedltcommitgt--ltpathgt82308203)
|`git add --patch`                                                                  |   pour choisir ce que l'on indexe|                [lien](https://git-scm.com/docs/git-add#Documentation/git-add.txt---patch)
|`git cherry-pick <hash>`                                                           |   récupérer un commit et l'appliquer sur `HEAD`|  [lien](https://git-scm.com/book/fr/v2/Commandes-Git-Patchs#_git_cherry_pick)
|`git commit --fixup=<hash>` puis `git rebase -i --autosquash --autostash <hash>^`  |   fondre un commit dans un précédent|
|`branch -av`                                                                       |   lister les branches|
|`commit --amend --no-edit`                                                         |   ajouté au commit précédent sans changer le message|
|`diff --ignore-all-space --word-diff`                                              |   afficher les changements aux mots (plutôt qu'à la ligne) avec `HEAD` sans tenir compte des espaces|
|`log --graph --oneline --decorate --since=7days --all`                             |   les commits de toutes les branches, regroupé et limité aux 7 derniers jours|


Votre avis compte!
------------------

Ne pas hésiter à proposer des mises à jour [pour cette page](https://gitlab.com/forga/process/fr/manuel/-/edit/production/docs/convention/git.md)
