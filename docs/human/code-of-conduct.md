Code de conduite
================


Introduction
------------

L'organisation s'attend à ce que chaque membre fasse preuve de respect et de courtoisie envers les autres membres de l'organisation et les personnes rencontrées dans le cadre de tout évènement organisé (formellement ou non) par l'organisation.

Ce texte une adaptation de la [charte AFPy](https://www.afpy.org/docs/charte) diffusée sous les termes de la [licence MIT](https://github.com/AFPy/site/blob/production/LICENSE).


Généralités
-----------

L'organisation souhaite éviter tout type de discrimination, que ce soit sur le sexe, l'orientation sexuelle, le handicap, l'apparence physique, l'origine ou la religion, et sous quelque forme que ce soit (parole, image, texte et autre). Bien évidemment, ceci n'est pas une liste exhaustive.

L'organisation reconnaît cependant la légitimité de minorités qui voudraient mener des actions de soutien à destination de leurs propres membres, en vue de favoriser le rééquilibrage de leur représentation au sein de la communauté entière.

Bien que l'organisation évolue dans le monde francophone, l'organisation s'engage à accueillir chacun, quelle que soit son langage ou son origine.

L'organisation attend de ses membres un langage et un style adaptés à une audience variée. Les allusions sexuelles ou à caractère raciste ne sont en aucun cas tolérées, quel que soit leur support.

Soyez poli·e. N'insultez personne. Discrimination et harcèlement sont à exclure aussi bien dans les écrits, les visuels, les paroles et les actes, et ce quelle que soit leur nature (sexuelle, raciste, religieuse, sociale, etc.).


Précisions
----------

### Définition de la discrimination

La discrimination inclut, selon la loi française :

> la situation dans laquelle, sur le fondement de son appartenance ou de sa non-appartenance, vraie ou supposée, à une ethnie ou une race, sa religion, ses convictions, son âge, son handicap, son orientation ou identité sexuelle ou son sexe, une personne est traitée de manière moins favorable qu'une autre ne l'est, ne l'a été ou ne l'aura été dans une situation comparable.

Sont donc proscrits :

* « tout agissement à connotation sexuelle, \[subi\] par une personne et ayant pour objet ou pour effet de porter atteinte à sa dignité ou de créer un environnement intimidant, hostile, dégradant, humiliant ou offensant » ;
* « le fait d'enjoindre à quiconque d'adopter un \[tel\] comportement ».


### Comportements proscrits

En référence à la définition qui précède, nous vous invitons à vous abstenir :
*   de toute expression ou attitude inappropriées,
*   de présenter des images sexuelles ou choquantes (notamment violentes),
*   d'intimidation délibérée, de harcèlement, des contacts physiques inconvenants et non consentis.


### Traitement des incidents

En cas de harcèlement ou de discrimination avéré, les représentant de l'organisateurs pourront prendre immédiatement les mesures qu'ils jugent appropriées pour faire cesser la situation et envisager les conséquences adéquates.


### Signaler

Si vous êtes harcelé(e) ou victime de discrimination, ou si vous êtes témoin de harcèlement ou de discrimination à l'égard d'autre(s) membr·e·s, ou si vous craignez la survenue d'un incident, contactez sans attendre un repésentant de l'organisation .


Votre avis compte!
------------------

Ne pas hésiter à proposer des mises à jour [pour cette page](https://gitlab.com/forga/process/fr/manuel/-/edit/production/docs/human/code-of-conduct.md)
