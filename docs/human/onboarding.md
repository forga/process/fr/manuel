Nouvelle arrivée
================

_(profil dev)_

Côté organisation
-----------------

* [ ]  Signature de la convention/contrat
* [ ]  Faire une place dans les locaux
* [ ]  Demander l'OS favoris entre Debian, Ubuntu & Windoz10
* [ ]  Installer l'OS sur sa future machine
* [ ]  Créer un _compte mail_
*  Ajouter le nouvel _user_
* [ ]  Créer un compte [Nexcloud](https://cloud.example.org/)
* [ ]  Créer un compte [GitLab](https://gitlab.com/users/sign_in)
    * [ ]  l'ajouter au groupe [`forga/devel`](https://gitlab.com/groups/forga/devel/-/group_members) sans oublier la date de fin de stage/mission/contrat si applicable
*  Prendre RdV avec l'équipe pour présentation rapide (15 min) le _jour J_ :
    * [ ]  Camille V.:      `HHMM`
    * [ ]  Dominique D.:    `HHMM`
    * [ ]  …            :   `HHMM`
* [ ]  Mettre à jour les dépôt de [`devel/third-party`](https://gitlab.com/forga/devel/third-party) avec leurs dépôts amonts
* [ ]  Envoyez les éléments de connexion au mail `@example.org` sur mail perso
* [ ]  Envoyez les autres éléments de connexion sur le mail `@example.org`


Côté collaborateur / mentor
---------------------------

Tout ce passe dans le projet [ `methode/embarquement`](https://gitlab.com/forga/process/fr/embarquement/).


Votre avis compte!
------------------

Ne pas hésiter à proposer des mises à jour :

* [de cette page](https://gitlab.com/forga/process/fr/manuel/-/edit/production/docs/human/onboarding.md)
* [des modèles](https://gitlab.com/-/ide/project/forga/process/fr/embarquement/edit/production/-/.gitlab/issue_templates) (embarquement & tutorat)
