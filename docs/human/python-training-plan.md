Plan de formation python web (proposition)
==========================================


| Semaine | Thème                        | Objet de l'apprentissage
| :-----: | :---:                        | :-----------------------
| 1       | _Bases_                      | Intégration du stagiaire, outils de travail (matériel, logiciel, [EDI][edi]), licences, etc.
| 2       | _Travail en équipe_          | Gestion de code source (avec [Git][git])
| 3       | _Travail en équipe_          | Conventions & usages (PEP8, formatage et tests) dans l'organisation _forga_
| 4       | _Relationnel client_         | Pratiques et usage dans l'organisation _forga_
| 5       | _Relationnel client_         | Analyse de besoin (avec [UML][uml]): règles de gestion et les processus
| 6       | _Relationnel client_         | Détailler les fonctionnalités et définir un modèle physique de donnée (avec [UML][uml])
| 7       | _Travail en équipe_          | Utilisation de [Gitlab][gitlab] dans l'organisation _forga_
| 8       | _Travail en équipe_          | Usage avancé de [Git][git], gestion de projets au long cours (exitant)
| 9       | _HTML/CSS/JS_                | Tour d'horizon des usages et pratiques dans l'organisation _forga_
| 10      | **Python** / [CLI][cli]      | Écriture de scripts en ligne de commande
| 11      | **Python** / [API REST][api] | Utilisation des APIs des services internes ([iTop][itop], [NextCloud][nextcloud] & [Gitlab][gitlab])
| 12      | **Python** / [API REST][api] | Réaliser des requêtes en lecture et écriture avec [Python][requests]
| 13      | **Python** / [Flask][flask]  | Mettre en place un service web basique (type `hello world !`)
| 14      | **Python** / [Flask][flask]  | Ajouter des fonctionnalités client
| 15      | **Python** / [Flask][flask]  | Créer un client d'API basique
| 16      | _Déploiement_                | Usage du shell et administration basique (linux)
| 17      | _Déploiement_                | Usage et administration de machines virtuelles
| 18      | **Python** / [Flask][flask]  | Héberger _Flask_ sur serveur de production
| 19      | **Python** / [Django][django]| Mettre en place un service web basique (type `hello world !`)
| 20      | **Python** / [Django][django]| Héberger _Django_ sur serveur de production
| 21      | _Déploiement_                | Automatisation du déploiement avec [Gitlab][gitlab] & [Ansible][ansible]
| 22      | **Python** / [Django][django]| Ajouter un système d'authentification utilisateur
| 23      | **Python** / [Django][django]| Utiliser la génération d'interface d'administration
| 24      | **Python** / [Django][django]| Structurer un projet en plusieurs applications réutilisable
| 25      | **Python** / [Django][django]| Créer un client d'API avancé


Votre avis compte!
------------------

Ne pas hésiter à proposer des mises à jour [pour cette page](https://gitlab.com/forga/process/fr/manuel/-/edit/production/docs/human/python-training-plan.md)


[ansible]: https://www.ansible.com/
[api]: https://en.wikipedia.org/wiki/Application_programming_interface
[cli]: https://en.wikipedia.org/wiki/Command-line_interface
[django]: https://djangoproject.com/
[edi]: https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement
[flask]: https://flask.palletsprojects.com/
[git]: https://git-scm.com/
[gitlab]: https://about.gitlab.com/
[itop]: https://www.itophub.io/
[nextcloud]: https://docs.nextcloud.com/server/stable/developer_manual/client_apis/OCS/index.html
[requests]: https://pypi.org/project/requests/
[uml]: https://en.wikipedia.org/wiki/Unified_Modeling_Language
