Informations légales
====================

Des informations légales sur notre organisation.


Votre avis compte!
------------------

Ne pas hésiter à proposer des mises à jour [pour cette page](https://gitlab.com/forga/process/fr/manuel/-/edit/production/docs/about/legal.md)
