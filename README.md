Manuel
======

Ce dépôt fait parti du groupe [`forga`](https://gitlab.com/forga/), pour plus d'informations sur ce groupe, visitez l'URL : [https://forga.gitlab.io/fr/](https://forga.gitlab.io/fr/)


À propos
--------

Ceci est un modèle de projet proposant une source de documentation unique accessible et éditable par tous. Il à été imaginé dans un contexte de TPE pour du développement utilisant `python` & `django` mais devrait pouvoir s'adapter à de nombreux autres contextes techniques.

Le contenu est publié à l'aide du générateur de [site statique](https://fr.wikipedia.org/wiki/Page_web_statique) [`MkDocs`](https://www.mkdocs.org/) sur les [pages statique de GitLab](https://about.gitlab.com/stages-devops-lifecycle/pages/).

Le projet original est disponible à cette URL : [`https://gitlab.com/forga/process/fr/manuel/`](https://gitlab.com/forga/process/fr/manuel/) sous les termes de la [_GNU Free Documentation License_](https://gitlab.com/forga/process/fr/manuel/-/blob/production/LICENSE.md), toute contribution est bienvenue!


Accéder au manuel
-----------------

🚀 [https://forga.gitlab.io/process/fr/manuel/](https://forga.gitlab.io/process/fr/manuel/)


Votre avis compte!
------------------

Ne pas hésiter à [proposer une mise à jour](https://gitlab.com/forga/process/fr/embarquement/-/edit/production/README.md) de cette page!
